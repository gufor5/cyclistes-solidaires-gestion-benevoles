<?php

use Illuminate\Support\Facades\Route;

use App\Database;
use App\Objects\Delivery;

// Set language to FR
App::setLocale('fr');

Auth::routes(['register' => true, 'reset' => true]);

Route::group(['middleware' => 'auth'], function () {

	// Home page related routes

	Route::get('/home', 'HomeController@home')->name('home');

	// Deliveries related routes

	foreach (Database::getEnabledProjects() as $project) 
	{
		Route::get($project->getUrl(), 'DeliveriesController@showDeliveries')->name($project->getIdentifier());
	}

	Route::post('/submitdelivery', 'DeliveriesController@submitDelivery')->name('submitdelivery');
	Route::post('/removedelivery', 'DeliveriesController@removeDelivery')->name('removedelivery');
	Route::post('/validatedelivery', 'DeliveriesController@validateDelivery')->name('validatedelivery');
	Route::post('/addvolunteer', 'DeliveriesController@addVolunteer')->name('addvolunteer');
	Route::post('/removevolunteer', 'DeliveriesController@removeVolunteer')->name('removevolunteer');

	// Projects related routes

	foreach (Database::getEnabledProjects() as $project) 
	{
		Route::get($project->getUrl().'/admin', 'ProjectsController@editProject')->name($project->getIdentifier().'admin');
	}

	Route::post('/addcoordonator', 'ProjectsController@addCoordonator')->name('addcoordonator');
	Route::post('/removecoordonator', 'ProjectsController@removeCoordonator')->name('removecoordonator');
	Route::post('/validatecoordonatoradd', 'ProjectsController@validateCoordonatorAdd')->name('validatecoordonatoradd');

	// Account management related routes

	Route::get('/logout', 'Auth\LoginController@logout');
	Route::get('/myaccount', 'VolunteersController@editVolunteer')->name('editVolunteer');
	Route::post('/myaccount/submitvolunteer', 'VolunteersController@submitVolunteer')->name('submitvolunteer');
	Route::post('/validatevolunteer', 'VolunteersController@validateVolunteer')->name('validatevolunteer');
	Route::get('/deletevolunteer', 'VolunteersController@deleteVolunteer')->name('deletevolunteer');

	// Dynamic database related routes

	Route::post('isVolunteerRegistered', 'DatabaseController@isVolunteerRegistered')->name('isVolunteerRegistered');
	Route::post('isDeliveryFull', 'DatabaseController@isDeliveryFull')->name('isDeliveryFull');
	Route::post('isTrailerNeeded', 'DatabaseController@isTrailerNeeded')->name('isTrailerNeeded');
	Route::post('isPasswordValid', 'DatabaseController@isPasswordValid')->name('isPasswordValid');

	// Exports related routes

	Route::get('exportUsers', 'ExportsController@exportUsers')->name('exportUsers');

	// Clear cache custom command
	Route::get('/clear-all-cache', function() {
	  Artisan::call('cache:clear');
	  Artisan::call('route:clear');
	  Artisan::call('view:clear');
	  Artisan::call('config:clear');
	  echo "Cleared all caches successfully.";
	});

});