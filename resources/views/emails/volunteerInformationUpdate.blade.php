@component('mail::message')
	
# Confirmation de vos nouvelles informations

<div> Nom: {{ $volunteer->name }} </div> 
<div> Email: {{ $volunteer->email }} </div>  
<div> Numéro de téléphone: {{ $volunteer->phonenumber }} </div>  
<div> Code postal: {{ $volunteer->postalcode }} </div>  
<br />

<div><b>Mes disponibilités approximatives</b></div>
@if (sizeof($volunteer->disponibilities) == 0)
<li><em>Aucune disponibilité sélectionnée</em></li>
@else
<ul>
	@foreach ($volunteer->disponibilities as $disponibility)
		<li>{{$disponibility}}</li>
	@endforeach
</ul>
@endif

<div><b>Mes équipements disponibles</b></div>
@if (sizeof($volunteer->capacity) == 0)
<li><em>Aucun équipement sélectionné</em></li>
@else
<ul>
	@foreach ($volunteer->capacity as $capacity)
		<li>{{$capacity}}</li>
	@endforeach
</ul>
@endif

<div><b>Mes quartiers intéressés</b></div>
@if (sizeof($volunteer->projects) == 0)
<li><em>Aucun quartier sélectionné</em></li>
@else
<ul>
	@foreach ($volunteer->projects as $project)
		<li>{{$project->getName()}}</li>
	@endforeach
</ul>
@endif

@endcomponent