@component('mail::message')

# Confirmation d'inscription

<div> Nom: {{ $volunteer->name }} </div> 
<div> Email: {{ $volunteer->email }} </div>  
<div> Numéro de téléphone: {{ $volunteer->phonenumber }} </div>  
<div> Code postal: {{ $volunteer->postalcode }} </div>  
<br />

@endcomponent