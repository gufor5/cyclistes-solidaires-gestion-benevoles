@component('mail::message')
# Avis de désinscription d'un bénévole

<div> Bénévole: {{ $volunteer->name }} </div> 
<div> Mission: {{ $delivery->getName() }} </div> 

@endcomponent