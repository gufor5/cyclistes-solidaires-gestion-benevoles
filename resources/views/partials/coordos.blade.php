<?php
    use App\Enums\VolunteerFormParametersEnum;
    use App\Enums\ProjectFormParametersEnum;
 ?>

<div class="app-coordo-container">

	<div class="app-coordo-header"> 
      <div class="app-coordo-header-label">	{{$coordo->name}}  
      </div> &nbsp;
	</div>

  <div class="app-coordo-body">

    <div class="app-coordo-informations">

			<div class="app-coordo-information"> 
				{{$coordo->email}} 
			</div> 

			<div class="app-coordo-information"> 
				{{$coordo->phonenumber}} 
			</div>

		</div>

    <div class="app-coordo-buttons">
    	<form action="{{ route('removecoordonator') }}" method="post" onsubmit="validateCoordonatorRemove();">

          @csrf

          <input type="hidden" name="redirect" value="{{ Route::getCurrentRoute()->getName() }}">

          <input type="hidden" name="{{VolunteerFormParametersEnum::ID}}" value="{{ $coordo->id }}">

          <input type="hidden" name="{{ProjectFormParametersEnum::IDENTIFIER}}" value="{{ $project->getIdentifier() }}">

          <button type="submit" class="app-coordo-button"> 
          	<i class="fas fa-trash"></i> 
          </button>

      </form>
		</div>

	</div>
</div>