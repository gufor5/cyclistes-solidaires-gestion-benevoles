<?php  
	Use App\Database;
?>

<div class="sidebar-container">
	
	<div class="sidebar-section">

		<div class="sidebar-section-label"> Liens utiles </div>

		<a href="https://wiki.lafabriquedesmobilites.fr/wiki/Cyclistes_solidaires" class="sidebar-link" target="_blank"> Page wiki </a>

		<a href="https://drive.google.com/file/d/1wgXMSCq5i91C6F3ZnOQtm_HqcA73gHHG/view?usp=sharing" class="sidebar-link" target="_blank"> Protocole sanitaire </a>

		<a href="https://www.flickr.com/photos/187615688@N07/albums/72157713638348246" class="sidebar-link" target="_blank"> Album flickr </a>

	</div>

	<div class="sidebar-section">

		<div class="sidebar-section-label"> Portails </div>

    @foreach(Database::getVisibleOnlyProjects() as $prj)

    	<a href="{{route($prj->getidentifier())}}" class="sidebar-link"> {{$prj->getName()}} </a>

    @endforeach 

	</div>

	@if ( Database::isCoordinator(Auth::user()->id) )
		<div class="sidebar-section">

			<div class="sidebar-section-label"> Coordonateurs </div>

			<a href="{{route('exportUsers')}}" class="sidebar-link"> Bénévoles inscrits </a>

			<a href="https://chat.fabmob.io/channel/cyclistessolidaires" class="sidebar-link" target="_blank"> Chat fabmob </a>

			<a href="https://docs.google.com/document/d/1VI-_snFDhGD9Kl3DoQx3bC56wI_-I6F9V7wXMFsUV2U/edit?usp=sharing" class="sidebar-link" target="_blank"> Guide d'utilisation </a>

		</div>
	@endif

	@if (Database::isProjectCoordinator($project->getIdentifier(), Auth::user()->id))
	
		<div class="sidebar-section">
			<div class="sidebar-section-label"> {{$project->getName()}} </div>

	    <a href="{{route($project->getIdentifier().'admin')}}" class="sidebar-link" > Droits de coordination </a>
		</div>

  @endif

	@if (Route::currentRouteName() == "editVolunteer")
		<div class="sidebar-section">
			
			<div class="sidebar-section-label"> Compte </div>

			<a href="{{route('deletevolunteer')}}" class="sidebar-link" onclick="confirmVolunteerDelete();"> Supprimer ce compte </a>

		</div>
	@endif

</div>