<div class="app-run-information">
	
	<div class="app-run-information-label"> {{$label}} </div>

	<div class="app-run-information-content"> {{$content}} </div>

</div>