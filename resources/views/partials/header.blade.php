<?php 
  use Illuminate\Support\Facades\Auth;

  use App\Database;
 ?>

<div class="app-header">
                
  <div class="app-header-title"> 
  	@yield('header-title')
	</div>

	<div class="app-header-userinfos">

    <div class="header-link"><i class="far fa-user"></i> 
      {{ Auth::user()->name }} 
    </div>
    <span style="padding-left:1vh;"></span>

		<a class="header-link" href="{{url('/myaccount')}}"><i class="fa fa-cog"></i></a>
    <span style="padding-left:1vh;"></span>

  	<a class="header-link" href="{{url('/logout')}}"><i class="fas fa-sign-out-alt"></i> </a>
  	<span style="padding-left:2vh;"></span>

	</div>

</div>