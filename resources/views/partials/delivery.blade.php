<?php
    use App\Objects\Delivery;
    use App\Enums\VolunteerRolesEnum;
    use App\Database;
 ?>

<div class="app-run-container">
                            
    <div class="app-run-header"> 
        
        <div class="app-run-header-label"> {{$delivery->getName()}} [{{$delivery->getIdealVolunteerNumber()}} personnes recherchées] </div> &nbsp;

        @if ( Database::isProjectCoordinator($project->getIdentifier(), Auth::user()->id) )
            <div class="app-run-label-buttons">
                
                <a 
                    class="app-run-label-button" 
                    data-toggle="modal" 
                    data-target="{{'#modalEditDelivery'}}"
                    data-type="edit"
                    data-id="{{$delivery->getId()}}"
                    data-project="{{$project->getIdentifier()}}"
                    data-ownerid="{{$delivery->getOwner()->id}}"
                    data-name="{{$delivery->getName()}}"
                    data-where="{{$delivery->getFromWhere()}}"
                    data-when="{{$delivery->getFromWhen()}}"
                    data-equipments="{{$delivery->getRequiredEquipments()}}"
                    data-objective="{{$delivery->getObjective()}}"
                    data-time="{{$delivery->getEstimatedTime()}}"
                    data-idealvolunteers="{{$delivery->getIdealVolunteerNumber()}}"
                    data-comment="{{$delivery->getComment()}}"
                    data-contactdelivery="{{$delivery->getContactDelivery()}}"
                >
                    <i class="fa fa-pencil-alt"></i> 
                </a> 

                <form action="{{ route('removedelivery') }}" method="post">
                    @csrf
                    <input type="hidden" name="deliveryid" value="{{ $delivery->getId() }}">
                    <button type="submit" class="app-run-label-button"> <i class="fas fa-trash"></i> </button>
                </form>

            </div>
        @endif

    </div>

    <div class="app-run-body">

        <div class="app-run-informations">

            @include('partials.deliveryInformation', array('label' => 'Quand', 'content' => $delivery->getFromWhen()))
            
            @include('partials.deliveryInformation', array('label' => 'Ou', 'content' => $delivery->getFromWhere()))

            @include('partials.deliveryInformation', array('label' => 'Quoi', 'content' => $delivery->getObjective()))

            @include('partials.deliveryInformation', array('label' => 'Temps estimé', 'content' => $delivery->getEstimatedTime() . ' minutes'))

            @include('partials.deliveryInformation', array('label' => 'Équipements', 'content' => $delivery->getRequiredEquipments()))

            @include('partials.deliveryInformation', array('label' => 'Contact', 'content' => $delivery->getContactDelivery()))

            @include('partials.deliveryInformation', array('label' => 'Notes', 'content' => $delivery->getComment()))

        </div>

        <div class="app-run-benevoles">
            
            <div class="app-run-benevoles-names">

                <div> <b> Bénévoles Inscrits </b> </div>

                @foreach ($delivery->getRegisteredVolunteers() as $volunteer)
                    
                    <div class="app-run-benevoles-name"> 

                        @if ( Database::isProjectCoordinator($project->getIdentifier(), Auth::user()->id) )

                            <a 
                                class="app-body-runs-label-new" 
                                data-toggle="modal" 
                                data-target="{{ '#modalShowVolunteers' . $delivery->getId() }}"
                            >
                                {{ $volunteer->name }} 
                            </a>  

                            @include('modals.modalShowVolunteers', array('delivery' => $delivery))

                        @else
                            <div class="app-body-runs-label-new">
                                {{ $volunteer->name }} 
                            </div>
                        @endif

                        @if ( $volunteer->id == Auth::id() 
                           || Database::isProjectCoordinator($project->getIdentifier(), Auth::user()->id) )

                            <form action="{{ route('removevolunteer') }}" method="post">
                                @csrf
                                <input type="hidden" name="deliveryid" value="{{ $delivery->getId() }}">
                                <input type="hidden" name="volunteerid" value="{{ $volunteer->id }}">
                                <button type="submit" class="app-run-benevoles-remove"> <i class="fas fa-user-minus"></i> </button>
                            </form>
                        @endif

                    </div>

                @endforeach

            </div>

            <form action="{{ route('addvolunteer') }}" method="post">
                @csrf
                <input type="hidden" name="deliveryid" value="{{ $delivery->getId() }}">
                <button 
                    type="submit" 
                    class="app-run-benevoles-add" 
                    data-toggle="modal" 
                    data-backdrop="true" 
                    data-target="#modalConfirm" 
                    onclick="validateVolunteerAdd('{{ $delivery->getId() }}')">
                    <i class="fas fa-user-plus"></i> &nbsp; me proposer! 
                </button>
            </form>

        </div>

    </div>

</div>