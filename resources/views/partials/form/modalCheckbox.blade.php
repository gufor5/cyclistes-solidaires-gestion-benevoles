
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script> 
	$(document).ready(function () {
    $(window).on("load", function(e) {

        setCheckBoxValue('#{{$id}}', '{{$checked}}');
    });
  }); 
</script>

<div class='form-row'>
   
  <label class='form-input-checkbox-label' for='{{$id}}' id='{{$id}}placeholder'> {{ $title }} </label>
  <input type='checkbox' id='{{$id}}' name='{{$name}}' class="form-input-checkbox" onchange="updateCheckBox('#{{$id}}')">
  <i class="fas fa-check form-input-checkbox-icon" id='{{$id}}icon'></i>

</div>
