
<div class='form-row'>

  <label class='form-input-label'> {{ $title }} </label>  

  <select name={{$name}} id={{$id}} class='form-input-field custom-select {{$name}}'>

    @for ($i = 0; $i < sizeof($array); $i++)

        <option> {{ $array[$i] }} </option>

    @endfor

  </select>

  <div class='form-input-feedback' id={{$id . 'feedback'}}></div>
  <div class='form-input-feedback-message form-validation-fail' id="{{$id . 'feedback-message'}}"></div>

</div>