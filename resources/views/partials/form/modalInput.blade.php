
<div class='form-row'>
   
  <label class='form-input-label' id="{{$id . 'label'}}"> {{ $title }} </label>
  <input type='{{ $input ?? "text" }}' id='{{$id}}' name='{{$name}}' value='{{$value}}' class="form-input-field" autocomplete="off" placeholder="{{ $placeholder}}">
	<div class='form-input-feedback' id="{{$id . 'feedback'}}"></div>
	<div class='form-input-feedback-message form-validation-fail' id="{{$id . 'feedback-message'}}"></div>

</div>