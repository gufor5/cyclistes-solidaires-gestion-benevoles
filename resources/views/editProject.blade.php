<?php
    use Illuminate\Support\Facades\Auth;

    use App\Objects\Delivery;
    use App\Objects\Project;
    use App\Enums\VolunteerFormParametersEnum;
    use App\Enums\ProjectFormParametersEnum;
    use App\Database;
 ?>

@extends('layouts.app')

@section('css')
    <link href="/css/project-styles.css" rel="stylesheet">
@endsection

@section('includes')
    @include('js.projectjs')
@endsection

@section('header-title')
    {{ strtoupper($project->getName()) }}
@endsection

@section('content')

	<div class="app-body-coordos">

		@if (Database::isProjectCoordinator($project->getIdentifier(), Auth::user()->id))

		  <div class="app-body-coordos-label">
		      Ajouter un coordonateur &nbsp;
			</div>

			<!-- BODY -->
		  <form class='app-body-coordos-form coordinatorAddForm' enctype="multipart/form-data" action="{{route('addcoordonator')}}" method='post' onsubmit='validateCoordonatorAddForm();' style="margin-bottom: 2vh">

		      Accordez les droits de coordination en indiquant l'adresse email liée au compte crée par cette personne sur la plateforme.

		      <div class="form-content">

		          @csrf

		          <input type="hidden" name="redirect" value="{{ Route::getCurrentRoute()->getName() }}">

		          <input type="hidden" name="{{ProjectFormParametersEnum::IDENTIFIER}}" id="{{ProjectFormParametersEnum::IDENTIFIER}}" value="{{ $project->getIdentifier() }}">

		          @include('partials.form.modalInput', ['placeholder' => "Addresse email", 'name' => VolunteerFormParametersEnum::EMAIL, 'id' => VolunteerFormParametersEnum::EMAIL, 'value' => "", 'title' => ""])

		      </div>

		      <!-- FOOTER -->

	        <button id='app-footer-submit' type='submit' class="app-coordo-button-add">
	        	</i> <i class="fas fa-user-plus"></i>
	        </button>

		  </form>

		  <div class="app-body-coordos-label">
		      Coordonateurs actuels &nbsp;
			</div>

			@foreach ($coordos as $coordo)

				@include('partials.coordos', array('coordo' => $coordo))

			@endforeach

		@else
		  <div class="app-body-coordos-label">
		    Page réservée aux coordonateurs de ce quartier
			</div>
		@endif
	
	</div>

@endsection
