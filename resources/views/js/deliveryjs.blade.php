<?php 
    Use App\Enums\DeliveryFormParametersEnum;
 ?>

<script>
    
    function validateVolunteerAdd(deliveryid)
    {
        // ALREADY REGISTERED CHECK 

        if (isAlreadyRegistered(deliveryid)) 
        {
            alert("Déjà inscrit!");

            event.preventDefault();
            event.stopPropagation();
            window.location.reload();
            return;
        }

        // DELIVERY FULL CHECK

        if (isDeliveryFull(deliveryid))
        {
            alert("Le nombre de bénévoles inscrits correspond déjà au nombre de bénévoles requis!");

            event.preventDefault();
            event.stopPropagation();
            window.location.reload();
            return;
        }

        // TRAILER NEEDED WARNING

        if (isTrailerNeeded(deliveryid))
        {
            alert("Étant donné la taille des chargements, une remorque à vélo est nécessaire sur cette mission. Sauf s'il vous a clairement été indiqué que cette dernière sera fournie par les Cyclistes Solidaires, votre inscription sous entends votre engagement à fournir votre propre remorque à vélo.");
        }
    }

    function isAlreadyRegistered(deliveryid)
    {
        var inputs = "pass=" + JSON.stringify({
            "deliveryid": deliveryid,
            "userid": "{{Auth::id()}}"
        });

        var output = false;

        $.ajax({
            type: 'POST',
            url: "/isVolunteerRegistered",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: inputs,
            cache: false,
            async: false,
            success: function(data) 
            {
                output = data;
            }
        });

        return output;
    }

    function isDeliveryFull(deliveryid)
    {
        var inputs = "pass=" + JSON.stringify({
            "deliveryid": deliveryid
        });

        var output = false;

        $.ajax({
            type: 'POST',
            url: "/isDeliveryFull",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: inputs,
            cache: false,
            async: false,
            success: function(data) 
            {
                output = data;
            }
        });

        return output;
    }

    function isTrailerNeeded(deliveryid)
    {
        var inputs = "pass=" + JSON.stringify({
            "deliveryid": deliveryid
        });

        var output = false;

        $.ajax({
            type: 'POST',
            url: "/isTrailerNeeded",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: inputs,
            cache: false,
            async: false,
            success: function(data) 
            {
                output = data;
            }
        });

        return output;
    }

    function validateDeliveryForm() 
    {
        // Get data from form
        var inputs = "pass=" + JSON.stringify({
            "{{DeliveryFormParametersEnum::ID}}": document.getElementById("{{DeliveryFormParametersEnum::ID}}").value,  
            "{{DeliveryFormParametersEnum::NAME}}": document.getElementById("{{DeliveryFormParametersEnum::NAME}}").value, 
            "{{DeliveryFormParametersEnum::WHERE}}": document.getElementById("{{DeliveryFormParametersEnum::WHERE}}").value, 
            "{{DeliveryFormParametersEnum::WHEN}}": document.getElementById("{{DeliveryFormParametersEnum::WHEN}}").value, 
            "{{DeliveryFormParametersEnum::OBJECTIVE}}": document.getElementById("{{DeliveryFormParametersEnum::OBJECTIVE}}").value, 
            "{{DeliveryFormParametersEnum::ESTIMATEDTIME}}": document.getElementById("{{DeliveryFormParametersEnum::ESTIMATEDTIME}}").value, 
            "{{DeliveryFormParametersEnum::EQUIPMENTS}}": document.getElementById("{{DeliveryFormParametersEnum::EQUIPMENTS}}").value,   
            "{{DeliveryFormParametersEnum::VOLUNTEERS_IDEAL}}": document.getElementById("{{DeliveryFormParametersEnum::VOLUNTEERS_IDEAL}}").value,
            "{{DeliveryFormParametersEnum::PROJECT}}": document.getElementById("{{DeliveryFormParametersEnum::PROJECT}}").value, 
            "{{DeliveryFormParametersEnum::COMMENT}}": document.getElementById("{{DeliveryFormParametersEnum::COMMENT}}").value,
            "{{DeliveryFormParametersEnum::CONTACT_DELIVERY}}": document.getElementById("{{DeliveryFormParametersEnum::CONTACT_DELIVERY}}").value    
          });

        // Call to validator
        var feedback;
        $.ajax({
            url: "/validatedelivery",
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: inputs,
            async: false,
            success: function(data) 
            {
                //feedback = data;
                feedback = JSON.parse(data);
            }
        });

        var results = new Array();
        results.push(processFeedback("{{DeliveryFormParametersEnum::NAME}}", feedback["{{DeliveryFormParametersEnum::NAME}}"]));
        results.push(processFeedback("{{DeliveryFormParametersEnum::WHERE}}", feedback["{{DeliveryFormParametersEnum::WHERE}}"]));
        results.push(processFeedback("{{DeliveryFormParametersEnum::WHEN}}", feedback["{{DeliveryFormParametersEnum::WHEN}}"]));
        results.push(processFeedback("{{DeliveryFormParametersEnum::OBJECTIVE}}", feedback["{{DeliveryFormParametersEnum::OBJECTIVE}}"]));
        results.push(processFeedback("{{DeliveryFormParametersEnum::ESTIMATEDTIME}}", feedback["{{DeliveryFormParametersEnum::ESTIMATEDTIME}}"]));
        results.push(processFeedback("{{DeliveryFormParametersEnum::EQUIPMENTS}}", feedback["{{DeliveryFormParametersEnum::EQUIPMENTS}}"]));
        results.push(processFeedback("{{DeliveryFormParametersEnum::VOLUNTEERS_IDEAL}}", feedback["{{DeliveryFormParametersEnum::VOLUNTEERS_IDEAL}}"]));

        if (results.indexOf(false) != -1)
        {
            event.preventDefault();
            event.stopPropagation();
        }
    }

</script>