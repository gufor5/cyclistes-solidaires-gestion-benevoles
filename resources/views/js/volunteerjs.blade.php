<?php 
    Use App\Enums\VolunteerFormParametersEnum;
 ?>

<script>

    function validateVolunteerForm() 
    {
        // Get data from form
        var inputs = "pass=" + JSON.stringify({
            "{{VolunteerFormParametersEnum::ID}}": document.getElementById("{{VolunteerFormParametersEnum::ID}}").value,  
            "{{VolunteerFormParametersEnum::NAME}}": document.getElementById("{{VolunteerFormParametersEnum::NAME}}").value, 
            "{{VolunteerFormParametersEnum::EMAIL}}": document.getElementById("{{VolunteerFormParametersEnum::EMAIL}}").value, 
            "{{VolunteerFormParametersEnum::PASSWORD}}": document.getElementById("{{VolunteerFormParametersEnum::PASSWORD}}").value, 
            "{{VolunteerFormParametersEnum::PHONENUMBER}}": document.getElementById("{{VolunteerFormParametersEnum::PHONENUMBER}}").value,
            "{{VolunteerFormParametersEnum::POSTALCODE}}": document.getElementById("{{VolunteerFormParametersEnum::POSTALCODE}}").value 
          });

        // Call to validator
        var feedback;
        $.ajax({
            url: "/validatevolunteer",
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: inputs,
            async: false,
            success: function(data) 
            {
                //feedback = data;
                feedback = JSON.parse(data);
            }
        });

        var results = new Array();
        results.push(processFeedback("{{VolunteerFormParametersEnum::NAME}}", feedback["{{VolunteerFormParametersEnum::NAME}}"]));
        results.push(processFeedback("{{VolunteerFormParametersEnum::EMAIL}}", feedback["{{VolunteerFormParametersEnum::EMAIL}}"]));
        results.push(processFeedback("{{VolunteerFormParametersEnum::PASSWORD}}", feedback["{{VolunteerFormParametersEnum::PASSWORD}}"]));
        results.push(processFeedback("{{VolunteerFormParametersEnum::PHONENUMBER}}", feedback["{{VolunteerFormParametersEnum::PHONENUMBER}}"]));
        results.push(processFeedback("{{VolunteerFormParametersEnum::POSTALCODE}}", feedback["{{VolunteerFormParametersEnum::POSTALCODE}}"]));

        if (results.indexOf(false) != -1)
        {
            event.preventDefault();
            event.stopPropagation();
        }
    }

    function confirmVolunteerDelete()
    {
        var input = prompt("Entrez votre mot de passe en majuscule pour confirmer la suppresion de votre compte.", "");

        if (validatePassword(input) == false)
        {
            event.preventDefault();
            event.stopPropagation();

            alert("Mot de passe invalide");
        }
    }

</script>