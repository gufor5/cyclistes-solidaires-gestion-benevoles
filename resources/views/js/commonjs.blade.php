<script>

    function validatePassword (password)
    {
        var inputs = "pass=" + JSON.stringify({
            "password": password
        });

        var output = false;

        $.ajax({
            type: 'POST',
            url: "/isPasswordValid",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: inputs,
            cache: false,
            async: false,
            success: function(data) 
            {
                output = data;
            }
        });

        return output;
    }

    function processFeedback (attribute, feedback)
    {
        if(feedback.status == true)
        {
            document.getElementById(attribute + "feedback").innerHTML = '<i class="far fa-check-circle"></i>';
            document.getElementById(attribute + "feedback").classList.remove("form-validation-fail");
            document.getElementById(attribute + "feedback").classList.add("form-validation-pass");

            document.getElementById(attribute + "feedback-message").innerHTML = '';
        }
        else if (feedback.status == "ignore")
        {
            document.getElementById(attribute + "feedback").innerHTML = '';
            document.getElementById(attribute + "feedback").classList.remove("form-validation-fail");
            document.getElementById(attribute + "feedback").classList.remove("form-validation-pass");

            document.getElementById(attribute + "feedback-message").innerHTML = '';
        }
        else
        {
            document.getElementById(attribute + "feedback").innerHTML = '<i class="far fa-times-circle"></i>';
            document.getElementById(attribute + "feedback").classList.remove("form-validation-pass");
            document.getElementById(attribute + "feedback").classList.add("form-validation-fail");

            document.getElementById(attribute + "feedback-message").innerHTML = feedback.feedback;
        }

        return feedback.status;
    }

    function resetForm(id)
    {
        $(id).find('form').trigger('reset');
        $(id).find('.form-input-feedback').html("");
        $(id).find('.form-input-feedback-message').html("");
    }

</script>