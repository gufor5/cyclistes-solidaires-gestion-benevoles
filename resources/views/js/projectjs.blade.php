<?php 
    Use App\Enums\ProjectFormParametersEnum;
    Use App\Enums\VolunteerFormParametersEnum;
 ?>

<script>
  
  function validateCoordonatorAddForm()
  {  
    // Get data from form
    var inputs = "pass=" + JSON.stringify({
        "{{ProjectFormParametersEnum::IDENTIFIER}}": document.getElementById("{{ProjectFormParametersEnum::IDENTIFIER}}").value,
        "{{VolunteerFormParametersEnum::EMAIL}}": document.getElementById("{{VolunteerFormParametersEnum::EMAIL}}").value
      });

    // Call to validator
    var feedback;
    $.ajax({
        url: "/validatecoordonatoradd",
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: inputs,
        async: false,
        success: function(data) 
        {
          feedback = JSON.parse(data);
        }
    });

    var results = new Array();
    results.push(processFeedback("{{VolunteerFormParametersEnum::EMAIL}}", feedback["{{VolunteerFormParametersEnum::EMAIL}}"]));

    if (results.indexOf(false) != -1)
    {
        event.preventDefault();
        event.stopPropagation();
    }
  }

  function validateCoordonatorRemove()
  {
    var input = prompt("Entrez votre mot de passe en majuscule pour confirmer le retrait de ce coordonateur.", "");

    if (validatePassword(input) == false)
    {
        event.preventDefault();
        event.stopPropagation();

        alert("Mot de passe invalide");
    }
  }

</script>