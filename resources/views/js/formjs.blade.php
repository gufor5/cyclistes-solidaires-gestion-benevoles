 
<script>

  /* CHECKBOX INPUT METHODS */

  function updateCheckBox(id)
  {
    if ($(id).is(":checked")) 
    {
      $(id+"icon").css('color', 'black');
    }
    else
    {
      $(id+"icon").css('color', 'transparent');
    }
  }

  function setCheckBoxValue(id, value)
  {
    $(id).prop('checked', value);
    updateCheckBox(id);
  }

</script>