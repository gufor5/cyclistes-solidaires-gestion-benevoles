<?php
    use Illuminate\Support\Facades\Auth;

    use App\Objects\Delivery;
    use App\Objects\Project;
    use App\Enums\VolunteerRolesEnum;
    use App\Enums\VolunteerFormParametersEnum;
    use App\Enums\VolunteerAvailabilityEnum;
    use App\Enums\VolunteerCapacityEnum;
    use App\Database;
 ?>

@extends('layouts.app')

@section('includes')
    @include('js.volunteerjs')
@endsection

@section('header-title')
    MON COMPTE
@endsection

@section('content')
    <!-- BODY -->
    <form class='form-form editVolunteerForm' enctype="multipart/form-data" action="{{route('submitvolunteer')}}" method='post' onsubmit='validateVolunteerForm();'>

        <div class="form-content">

            @csrf
            
            <input type="hidden" name="{{VolunteerFormParametersEnum::ID}}" id="{{VolunteerFormParametersEnum::ID}}" value="{{Auth::user()->id}}">

            @include('partials.form.modalTitle', ['title' => "Mes informations"])

            @include('partials.form.modalInput', ['placeholder' => "Nom", 'name' => VolunteerFormParametersEnum::NAME, 'id' => VolunteerFormParametersEnum::NAME, 'value' => Auth::user()->name, 'title' => ""])

            @include('partials.form.modalInput', ['placeholder' => "Mot de passe", 'name' => VolunteerFormParametersEnum::PASSWORD, 'id' => VolunteerFormParametersEnum::PASSWORD, 'value' => '', 'title' => ""])

            Remplissez le champs mot de passe pour changer votre mot de passe. Laissez ce champs libre si vous ne désirez pas éditer votre mot de passe.

            @include('partials.form.modalTitle', ['title' => "Mes identifiants"])

            @include('partials.form.modalInput', ['placeholder' => "Email", 'name' => VolunteerFormParametersEnum::EMAIL, 'id' => VolunteerFormParametersEnum::EMAIL, 'value' => Auth::user()->email, 'title' => ""])

            @include('partials.form.modalInput', ['placeholder' => "Numéro de téléphone", 'name' => VolunteerFormParametersEnum::PHONENUMBER, 'id' => VolunteerFormParametersEnum::PHONENUMBER, 'value' => Auth::user()->phonenumber, 'title' => ""])

            @include('partials.form.modalInput', ['placeholder' => "Code postal", 'name' => VolunteerFormParametersEnum::POSTALCODE, 'id' => VolunteerFormParametersEnum::POSTALCODE, 'value' => Auth::user()->postalcode, 'title' => ""])

            @include('partials.form.modalTitle', ['title' => "Mes équipements disponibles"])

            @foreach (VolunteerCapacityEnum::allValues() as $capacity)

                <?php 
                    $index = VolunteerFormParametersEnum::CAPACITY . str_replace(' ', '', $capacity);
                    $checked = in_array($capacity, json_decode(Auth::user()->capacity)); ?>

                @include('partials.form.modalCheckbox', ['title' => $capacity, 'name' => $index, 'id' => $index, 'checked' => $checked])

            @endforeach

            @include('partials.form.modalTitle', ['title' => "Mes disponibilités approximatives"])

            @foreach (VolunteerAvailabilityEnum::allValues() as $availability)

                <?php 
                    $index = VolunteerFormParametersEnum::AVAILABILITIES . str_replace(' ', '', $availability);
                    $checked = in_array($availability, json_decode(Auth::user()->disponibilities)); ?>

                @include('partials.form.modalCheckbox', ['title' => $availability, 'name' => $index, 'id' => $index, 'checked' => $checked])

            @endforeach

            @include('partials.form.modalTitle', ['title' => "Mes quartiers intéressés"])

            @foreach (Database::getVisibleOnlyProjects() as $project)

                <?php 
                    $index = VolunteerFormParametersEnum::PROJECTS . $project->getIdentifier();
                    $checked = in_array($project->getIdentifier(), json_decode(Auth::user()->projects)); ?>

                @include('partials.form.modalCheckbox', ['title' => $project->getName(), 'name' => $index, 'id' => $index, 'checked' => $checked])

            @endforeach

        </div>

        <!-- FOOTER -->
        <div class='form-buttons-row'>

            <button id='app-footer-submit' type='submit' class="form-button"><i class="form-button-icon fa fa-check-circle"></i> Enregistrer</button>

            <button id='app-footer-close' type='button' class='form-button' onclick='window.location.href = "{{route('home')}}";'><i class="form-button-icon fa fa-undo"></i> Retour</button>

        </div>

    </form>
@endsection
