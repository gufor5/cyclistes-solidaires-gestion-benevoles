
<?php
    use Illuminate\Support\Facades\Auth;
    use App\Objects\Delivery;
    use App\Objects\Project;
    use App\Enums\VolunteerRolesEnum;
    use App\Database;
 ?>

@extends('layouts.app')

@section('css')
    <link href="/css/delivery-styles.css?v={{ time() }}" rel="stylesheet">
@endsection

@section('includes')
    @include('modals.modalEditDelivery')
    @include('js.deliveryjs')
@endsection

@section('header-title')
    {{ strtoupper($project->getName()) }}
@endsection

@section('content')
    
    <div class="app-body-runs-label">
        Livraisons à venir &nbsp;

        @if (Database::isProjectCoordinator($project->getIdentifier(), Auth::user()->id))

            <a 
                class="app-body-runs-label-new" 
                data-toggle="modal" 
                data-target="{{'#modalEditDelivery'}}"
                data-type="new"
                data-project="{{$project->getIdentifier()}}"
                data-ownerid="{{Auth::user()->id}}"
            >
                <i class="fa fa-plus-circle"></i> 
            </a>  

        @endif
    </div>

    <div class="app-body-runs">
        @foreach($deliveries as $delivery) 

            @include('partials.delivery', array('delivery' => $delivery))

        @endforeach
    </div>

@endsection

        