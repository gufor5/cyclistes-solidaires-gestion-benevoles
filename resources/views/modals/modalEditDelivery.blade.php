<?php  
  use App\Objects\Delivery;
  use App\Enums\DeliveryFormParametersEnum;
  use App\Enums\DeliveryEquipmentsEnum;
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>  

  $(document).ready(function () {

    $(document).on("show.bs.modal", function(e) 
    {
      $(this).find("#{{DeliveryFormParametersEnum::PROJECT}}").val($(e.relatedTarget).data('project'));
      $(this).find("#{{DeliveryFormParametersEnum::OWNER}}").val($(e.relatedTarget).data('ownerid'));

      if ($(e.relatedTarget).data('type') == "new")
      {
        $(this).find("#modal-edit-title").text("Nouvelle livraison");

        $(this).find("#{{DeliveryFormParametersEnum::ID}}").val(-1);
        $(this).find("#{{DeliveryFormParametersEnum::NAME}}").val("");
        $(this).find("#{{DeliveryFormParametersEnum::WHERE}}").val("");
        $(this).find("#{{DeliveryFormParametersEnum::WHEN}}").val("");
        $(this).find("#{{DeliveryFormParametersEnum::OBJECTIVE}}").val("");
        $(this).find("#{{DeliveryFormParametersEnum::ESTIMATEDTIME}}").val("");
        $(this).find("#{{DeliveryFormParametersEnum::EQUIPMENTS}}").val("{{DeliveryEquipmentsEnum::BACKPACK}}");
        $(this).find("#{{DeliveryFormParametersEnum::VOLUNTEERS_IDEAL}}").val("");
        $(this).find("#{{DeliveryFormParametersEnum::COMMENT}}").val("");
        $(this).find("#{{DeliveryFormParametersEnum::CONTACT_DELIVERY}}").val("");
      }
      else
      {
        $(this).find("#modal-edit-title").text($(e.relatedTarget).data('name'));

        /*var date = new Date($(e.relatedTarget).data('when'));

        var converted = date.getFullYear() + "-" + ("0"+(date.getMonth()+1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2) + "T" + ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2);*/

        //"2013-03-18T13:00"

        $(this).find("#{{DeliveryFormParametersEnum::ID}}").val($(e.relatedTarget).data('id'));
        $(this).find("#{{DeliveryFormParametersEnum::NAME}}").val($(e.relatedTarget).data('name'));
        $(this).find("#{{DeliveryFormParametersEnum::WHERE}}").val($(e.relatedTarget).data('where'));
        $(this).find("#{{DeliveryFormParametersEnum::WHEN}}").val($(e.relatedTarget).data('when'));
        $(this).find("#{{DeliveryFormParametersEnum::OBJECTIVE}}").val($(e.relatedTarget).data('objective'));
        $(this).find("#{{DeliveryFormParametersEnum::ESTIMATEDTIME}}").val($(e.relatedTarget).data('time'));
        $(this).find("#{{DeliveryFormParametersEnum::EQUIPMENTS}}").val($(e.relatedTarget).data('equipments'));
        $(this).find("#{{DeliveryFormParametersEnum::VOLUNTEERS_IDEAL}}").val($(e.relatedTarget).data('idealvolunteers'));
        $(this).find("#{{DeliveryFormParametersEnum::COMMENT}}").val($(e.relatedTarget).data('comment'));
        $(this).find("#{{DeliveryFormParametersEnum::CONTACT_DELIVERY}}").val($(e.relatedTarget).data('contactdelivery'));
      }
    });

    $(document).on('hidden.bs.modal', function () 
    {
      resetForm(this);
    });
  });
  
</script>

<div class="modal-container" id="modalEditDelivery" tabindex="0" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

  <!-- Modal content -->
  <div class="edit-modal-content modal-content" role='document'>

    <!-- HEADER -->
    <div class='modal-header'>
      <label class='modal-header-title' id='modal-edit-title'> </label>
      <button type='button' class='modal-header-close' onclick="$('#modalEditDelivery').modal('hide');" aria-hidden='true'>×</button>
    </div>

    <!-- BODY -->
    <form class="modal-body editDeliveryForm" enctype="multipart/form-data" action="{{route('submitdelivery')}}" method="POST" onsubmit="validateDeliveryForm();">

      @csrf
      
      <div class="form-content">

        <input type="hidden" name="{{DeliveryFormParametersEnum::ID}}" id="{{DeliveryFormParametersEnum::ID}}" value="">
        <input type="hidden" name="{{DeliveryFormParametersEnum::PROJECT}}" id="{{DeliveryFormParametersEnum::PROJECT}}" value="">
        <input type="hidden" name="{{DeliveryFormParametersEnum::OWNER}}" id="{{DeliveryFormParametersEnum::OWNER}}" value="">
        
        @include('partials.form.modalTitle', ['title' => "Nom de la mission"])

        @include('partials.form.modalInput', ['placeholder' => "", 'name' => DeliveryFormParametersEnum::NAME, 'id' => DeliveryFormParametersEnum::NAME, 'value' => "", 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Lieu de rendez-vous"])

        @include('partials.form.modalInput', ['placeholder' => "", 'name' => DeliveryFormParametersEnum::WHERE, 'id' => DeliveryFormParametersEnum::WHERE, 'value' => "", 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Date de rendez-vous (format: yyyy-mm-dd HH:MM exemple: 2020-05-20 09:00)"])

        @include('partials.form.modalInput', ['placeholder' => "", 'name' => DeliveryFormParametersEnum::WHEN, 'id' => DeliveryFormParametersEnum::WHEN, 'value' => "", 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Objectif de la livraison"])

        @include('partials.form.modalInput', ['placeholder' => "", 'name' => DeliveryFormParametersEnum::OBJECTIVE, 'id' => DeliveryFormParametersEnum::OBJECTIVE, 'value' => "", 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Temps estimé requis en minutes"])

        @include('partials.form.modalInput', ['placeholder' => "", 'name' => DeliveryFormParametersEnum::ESTIMATEDTIME, 'id' => DeliveryFormParametersEnum::ESTIMATEDTIME, 'value' => "", 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Équippements requis"])

        @include('partials.form.modalSelect', ['name' => DeliveryFormParametersEnum::EQUIPMENTS, 'id' => DeliveryFormParametersEnum::EQUIPMENTS, 'array' => DeliveryEquipmentsEnum::allValues(), 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Nombre idéal de voluntaires"])

        @include('partials.form.modalInput', ['placeholder' => "", 'name' => DeliveryFormParametersEnum::VOLUNTEERS_IDEAL, 'id' => DeliveryFormParametersEnum::VOLUNTEERS_IDEAL, 'value' => "", 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Contact"])

        @include('partials.form.modalInput', ['input' => 'email' ,'placeholder' => "", 'name' => DeliveryFormParametersEnum::CONTACT_DELIVERY, 'id' => DeliveryFormParametersEnum::CONTACT_DELIVERY, 'value' => "", 'title' => ""])

        @include('partials.form.modalTitle', ['title' => "Note supplémentaires"])

        @include('partials.form.modalInput', ['placeholder' => "", 'name' => DeliveryFormParametersEnum::COMMENT, 'id' => DeliveryFormParametersEnum::COMMENT, 'value' => "", 'title' => ""])
        

      </div>

      <!-- FOOTER -->

      <div class='form-buttons-row'>
        <button id='modal-footer-submit' type='submit' class="form-button"><i class="form-button-icon fa fa-check-circle"></i> Enregistrer</button>
        <button id='modal-footer-close' type='button' class='form-button' onclick="$('#modalEditDelivery').modal('hide');"><i class="form-button-icon fa fa-ban"></i> Annuler</button>
      </div>

    </form>

  </div>
</div>
