<?php  
  use App\Objects\Delivery;
  use App\Enums\DeliveryFormParametersEnum;
  use App\Enums\DeliveryEquipmentsEnum;
?>

<?php  
  $modalid = 'modalShowVolunteers' . $delivery->getId();
?>

<div class="modal-container" id="{{$modalid}}" tabindex="0" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

  <!-- Modal content -->
  <div class="show-modal-content modal-content" role='document'>

    <!-- HEADER -->
    <div class='modal-header'>
      <label class='modal-header-title' id='modal-edit-title'> {{ $delivery->getName() }} </label>
      <button type='button' class='modal-header-close' onclick="$('#{{$modalid}}').modal('hide');" aria-hidden='true'>×</button>
    </div>

    <!-- BODY -->
      
    <div class="modal-body">

      @foreach ($delivery->getRegisteredVolunteers() as $volunteer)

        <p>
          <b> {{ $volunteer->name }} </b>
          <div> {{ $volunteer->email }} </div>
          <div> {{ $volunteer->phonenumber }} </div>
        </p>

      @endforeach

    </div>

    <!-- FOOTER -->

    <div class='modal-footer'>
      <button id='modal-footer-close' type='button' class='modal-footer-button' onclick="$('#{{$modalid}}').modal('hide');"> Ok </button>
    </div>

  </div>
</div>
