<?php 
    Use App\User;
 ?>
 
@extends('layouts.auth')

@section('content')
    <form class="form-form" method="POST" action="{{ route('password.update') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-row">
            <label><strong>Choix du nouveau mot de passe</strong></label>
        </div>

        <div class="form-row">
            <input id="email" type="email" class="form-input-field" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Adresse email">
        </div>

        <div class="form-row">
            <input id="password" type="password" class="form-input-field" name="password" required autocomplete="new-password" placeholder="Nouveau mot de passe">
        </div>

        <div class="form-row">
            <input id="password-confirm" type="password" class="form-input-field" name="password_confirmation" required autocomplete="new-password" placeholder="Confirmation du nouveau mot de passe">
        </div>


        <div class="form-buttons-col">
            <button type="submit" class="form-button">
                Envoyer
            </button>

            <a class="form-button" href="{{ route('login') }}">
                Retour
            </a> 
        </div>

        <div class="auth-feedbacks">
            @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror

            @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
    </form>
</div>
@endsection
