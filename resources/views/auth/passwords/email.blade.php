<?php 
    Use App\User;
 ?>

@extends('layouts.auth')

@section('content')
    <form class="form-form" method="POST" action="{{ route('password.email') }}">
        @csrf

        <div class="form-row">
            <label><strong>Réinitialisation du mot de passe</strong></label>
        </div>

        <div class="form-row">
            <input id="email" type="email" class="form-input-field" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Adresse email">
        </div>

        <div class="form-buttons-col">

                <button type="submit" class="form-button">
                    Envoyer
                </button>

                <a class="form-button" href="{{ route('login') }}">
                    Retour
                </a> 
        </div>

        <div class="auth-feedbacks">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>Adresse email invalide</strong>
                </span>
            @enderror

            @if (session('status'))
                <span class="invalid-feedback" role="alert">
                    <strong>Lien de réinitialisation envoyé</strong>
                </span>
            @endif
        </div>
    </form>
@endsection
