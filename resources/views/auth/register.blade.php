
<?php
    use App\Database;
    use App\Enums\VolunteerAvailabilityEnum;
    use App\Enums\VolunteerCapacityEnum;
 ?>

@extends('layouts.auth')

@section('content')
    <form class="form-form" method="POST" action="{{ route('register') }}">
        @csrf

        @include('partials.form.modalTitle', ['title' => "Conditions d'utilisation"])

        <div style="text-align: left; width: 100%; margin-top: 1vh;">
            En vous inscrivant sur notre plateforme, vous prennez connaissance de notre <a href="https://drive.google.com/file/d/1eBXSMWkKcl_gZ7H_fC_GqVQQmUSAvyvg/view?usp=sharing" class="sidebar-link" target="_blank">formulaire de décharge</a> et consentez à respecter notre <a href="https://drive.google.com/file/d/1wgXMSCq5i91C6F3ZnOQtm_HqcA73gHHG/view?usp=sharing" class="sidebar-link" target="_blank">protocole sanitaire</a>.
        </div>

        @include('partials.form.modalTitle', ['title' => "Mes identifiants"])

        <div class="form-row">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror form-input-field" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email">
        </div>

        <div class="form-row">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror form-input-field" name="password" value="{{ old('password') }}" placeholder="Mot de passe" required autocomplete="password" autofocus>
        </div>

        @include('partials.form.modalTitle', ['title' => "Mes informations"])

        <div class="form-row">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror form-input-field" name="name" value="{{ old('name') }}" placeholder="Nom" required autocomplete="name" autofocus>
        </div>

        <div class="form-row">
            <input id="phonenumber" type="phonenumber" class="form-control @error('phonenumber') is-invalid @enderror form-input-field" name="phonenumber" placeholder="Cellullaire" required autocomplete="phonenumber">
        </div>

        <div class="form-row">
            <input id="postalcode" type="postalcode" class="form-control @error('postalcode') is-invalid @enderror form-input-field" name="postalcode" placeholder="Code postal" required>
        </div>

        @include('partials.form.modalTitle', ['title' => "Mes équipements disponibles"])

        <div class="form-column">
            @foreach (VolunteerCapacityEnum::allValues() as $capacity)

                @include('partials.form.modalCheckbox', ['title' => $capacity, 'name' => 'capacity' . str_replace(' ', '', $capacity), 'id' => 'capacity' . str_replace(' ', '', $capacity), 'checked' => '']) 

            @endforeach
        </div>

        @include('partials.form.modalTitle', ['title' => "Mes disponibilités approximatives"])

        <div class="form-column">
            @foreach (VolunteerAvailabilityEnum::allValues() as $availability)

                @include('partials.form.modalCheckbox', ['title' => $availability, 'name' => 'availability' . str_replace(' ', '', $availability), 'id' => 'availability' . str_replace(' ', '', $availability), 'checked' => '']) 

            @endforeach
        </div>

        @include('partials.form.modalTitle', ['title' => "Mes quartiers intéressés"])

        <div class="form-column">
            @foreach (Database::getVisibleOnlyProjects() as $project)
                    
                @include('partials.form.modalCheckbox', ['title' => $project->getName(), 'name' => 'project'. $project->getIdentifier(), 'id' => 'project' . $project->getIdentifier(), 'checked' => ''])

            @endforeach
        </div>

        <!--
        <div class="form-group row auth-row">

            <div class="col-md-6">
                <input id="password" type="hidden" class="form-control @error('password') is-invalid @enderror auth-textbox" name="password" placeholder="Mot de passe" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="form-group row auth-row">

            <div class="col-md-6">
                <input id="password-confirm" type="hidden" class="form-control auth-textbox" name="password_confirmation" placeholder="Confirmation mot de passe" required autocomplete="new-password">
            </div>
        </div>-->

        <div class="form-buttons-col">
            <button type="submit" class="form-button">
                    Enregistrer
            </button>
            <a class="form-button" href="{{ route('login') }}">
                    Retour
            </a> 
        </div>

        <div class="auth-feedbacks">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            @error('postalcode')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            @error('phonenumber')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </form>
@endsection