<?php 
    Use App\User;
 ?>

@extends('layouts.auth')

@section('content')
    <form class="form-form" method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-row">
            <input id="email" type="email" class="form-input-field" name="email" placeholder="Adresse email" required autocomplete="current-email">
        </div>

        <div class="form-row">
            <input id="password" type="password" class="form-input-field" name="password" placeholder="Mot de passe" required autocomplete="current-password">
        </div>

        <div class="form-buttons-col">
            <button type="submit" class="form-button">
                Se connecter
            </button>

            @if (Route::has('password.request'))
                <a class="form-button" href="{{ route('password.request') }}">
                    Mot de passe oublié?
                </a>
            @endif

            <a class="form-button" href="{{ route('register') }}">
                Créer un compte
            </a> 
        </div>

        <div class="auth-feedbacks">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong> Identifiants invalides </strong>
                </span>
            @enderror

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong> Identifiants invalides </strong>
                </span>
            @enderror
        </div>
    </form>

    <div style="border: 1px solid gray; width: 100%; border-radius: 5px">
        <p>Nous avons un nouveau nom de domaine! </p>
        <p>Assurez vous d'utiliser notre nouveau site web: </p>
        <p><a href="https://www.cyclistes-solidaires.org/home" class="sidebar-link">www.cyclistes-solidaires.org/home</span></p>
    </div>

@endsection
