# Cycliste solidaires gestion benevoles

This applications is the main project for [www.cycliste-solidaire.org](https://www.cyclistes-solidaires.org/home). 

## Getting started

To test the application locally you can do the following command 

```
$ docker-compose up -d
## Migration database
$ docker-compose exec laravel php artisan migrate
```

### Database dump

If you like to have real data. Get the username database and password in the heroku dashboard. Get the dump of the database 

```
$ pg_dump -s -d postgres://username:password@ec2-34-193-232-231.compute-1.amazonaws.com:5432/d97v3ve7beqj10 > schema-cycliste.sql
$ mv schema-cycliste.sql build/docker-entrypoint-initdb.d/schema-cycliste.sql
$ docker-compose down -v 
$ docker-compose up -d 
```


## Deployment Production

There is no CI/CD for the moment. The deployment is manuel

### Pushing the latest code in production

```
$ heroku git:remote --app cyclistes-solidaires-montreal
$ git push heroku master
```

### Migrate database 

```
$ heroku run bash --app cyclistes-solidaires-montreal
$ php artisan migrate
```

Rollback 

```
$ php artisan migrate:rollback --step=1
```