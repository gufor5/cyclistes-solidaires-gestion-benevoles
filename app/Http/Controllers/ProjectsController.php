<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;

use App\Database;
use App\User;
use App\Validator;
use App\Enums\VolunteerFormParametersEnum;
use App\Enums\ProjectFormParametersEnum;

use App\Mail\CoordonatorRemoveConfirmation;
use App\Mail\CoordonatorAddConfirmation;

class ProjectsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function editProject()
    {
        $projectidentifier = Route::getCurrentRoute()->getName();

        $projectidentifier = str_replace("admin", "", $projectidentifier);

        $project = Database::getProject($projectidentifier);

        $coordos = Database::getProjectCoordos($projectidentifier);

        return view('editProject', ['project' => $project, 'coordos' => $coordos]);
    }

    public function addCoordonator(Request $request)
    {
        $project = Database::getProject($request->input(ProjectFormParametersEnum::IDENTIFIER));

        $user = Database::getVolunteer($request->input(VolunteerFormParametersEnum::EMAIL));

        Database::addCoordonatorToProject($project->getIdentifier(), $user->id);

        Mail::to($user->email)->send(new CoordonatorAddConfirmation($project, $user));

        return redirect(route($request->input('redirect')));
    }

    public function removeCoordonator(Request $request)
    {
        $project = Database::getProject($request->input(ProjectFormParametersEnum::IDENTIFIER));

        $user = User::find($request->input(VolunteerFormParametersEnum::ID));

        Database::removeCoordonatorFromProject($project->getIdentifier(), $user->id);

        Mail::to($user->email)->send(new CoordonatorRemoveConfirmation($project, $user));

        return redirect(route($request->input('redirect')));
    }

    public function validateCoordonatorAdd(Request $request)
    {
        $pass = $request->input("pass");
        $passArray = json_decode($pass, true);

        $validator = new Validator();

        $volunteer = new User();
        $volunteer->email = $passArray[VolunteerFormParametersEnum::EMAIL];

        $coordos = Database::getProjectCoordos($passArray[ProjectFormParametersEnum::IDENTIFIER]);

        return json_encode($validator->validateCoordonatorAdd($coordos, $volunteer));
    }
}
