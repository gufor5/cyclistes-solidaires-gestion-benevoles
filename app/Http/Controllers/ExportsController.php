<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\UsersExport;

use Maatwebsite\Excel\Facades\Excel;

class ExportsController extends Controller
{
  public function exportUsers(Request $request) 
  {
    return Excel::download(new UsersExport, 'users' . date('_Ymd_His') . '.xlsx');
  }
}