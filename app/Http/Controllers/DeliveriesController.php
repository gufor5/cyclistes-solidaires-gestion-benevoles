<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;

use App\Database;
use App\User;
use App\Validator;
use App\Enums\DeliveryFormParametersEnum;
use App\Objects\Delivery;
use App\Mail\VolunteerRemoveFromDelivery;
use App\Mail\VolunteerAddedToDelivery;

class DeliveriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function showDeliveries(Request $request)
    {
		$projectidentifier = Route::getCurrentRoute()->getName();
		$deliveries = Database::getDeliveriesAfterYesterday($projectidentifier);
		$project = Database::getProject($projectidentifier);

        return view('deliveries', ['project' => $project, 'deliveries' => $deliveries]);
    }

    public function submitDelivery(Request $request)
    {
        $delivery = new Delivery(
            $request->input(DeliveryFormParametersEnum::ID),
            $request->input(DeliveryFormParametersEnum::NAME),
            User::where('id', $request->input(DeliveryFormParametersEnum::OWNER))->first(),
            $request->input(DeliveryFormParametersEnum::WHERE),
            $request->input(DeliveryFormParametersEnum::WHEN),
            $request->input(DeliveryFormParametersEnum::OBJECTIVE),
            $request->input(DeliveryFormParametersEnum::ESTIMATEDTIME),
            $request->input(DeliveryFormParametersEnum::EQUIPMENTS),
            array(),
            $request->input(DeliveryFormParametersEnum::VOLUNTEERS_IDEAL),
            $request->input(DeliveryFormParametersEnum::PROJECT),
            $request->input(DeliveryFormParametersEnum::COMMENT),
            $request->input(DeliveryFormParametersEnum::CONTACT_DELIVERY),
        );

        if (Database::isDeliveryExist($request->input(DeliveryFormParametersEnum::ID)))
        {
            Database::updateDelivery($delivery);
        }
        else
        {
            Database::insertDelivery($delivery);
        }

        return redirect(url()->previous());
    }

    public function removeDelivery(Request $request)
    {
        Database::removeDelivery($request->input("deliveryid"));

    	return redirect(url()->previous());
    }

    public function validateDelivery(Request $request)
    {
        $pass = $request->input("pass");
        $passArray = json_decode($pass, true);

        $validator = new Validator();
        $delivery = new Delivery(
            $passArray[DeliveryFormParametersEnum::ID],
            $passArray[DeliveryFormParametersEnum::NAME],
            null,
            $passArray[DeliveryFormParametersEnum::WHERE],
            $passArray[DeliveryFormParametersEnum::WHEN],
            $passArray[DeliveryFormParametersEnum::OBJECTIVE],
            $passArray[DeliveryFormParametersEnum::ESTIMATEDTIME],
            $passArray[DeliveryFormParametersEnum::EQUIPMENTS],
            array(),
            $passArray[DeliveryFormParametersEnum::VOLUNTEERS_IDEAL],
            $passArray[DeliveryFormParametersEnum::PROJECT],
            $passArray[DeliveryFormParametersEnum::COMMENT],
            $passArray[DeliveryFormParametersEnum::CONTACT_DELIVERY]);

        return json_encode($validator->validateDelivery($delivery));
    }

    public function addVolunteer(Request $request)
    {
        Database::addVolunteerToDelivery($request->input("deliveryid"), Auth::id());

        $delivery = Database::getDelivery($request->input("deliveryid"));

        $volunteer = User::where('id', Auth::id())->first();

        Mail::to($delivery->getOwner()->email)->send(new VolunteerAddedToDelivery($delivery, $volunteer));

    	return redirect(url()->previous());
    }

    public function removeVolunteer(Request $request)
    {
        Database::removeVolunteerToDelivery(
            $request->input("deliveryid"),
            $request->input("volunteerid"));

        $delivery = Database::getDelivery($request->input("deliveryid"));

        $volunteer = User::where('id', $request->input("volunteerid"))->first();

        Mail::to($delivery->getOwner()->email)->send(new VolunteerRemoveFromDelivery($delivery, $volunteer));

    	return redirect(url()->previous());
    }
}
