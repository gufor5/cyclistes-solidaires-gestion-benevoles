<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Database;

class DatabaseController extends Controller
{
  public function isVolunteerRegistered(Request $request) 
  {
  	$data = $request->input("pass");
    $dataArray = json_decode($data, true);

    $deliveryid = $dataArray["deliveryid"];
    $userid = $dataArray["userid"];

    return Database::isVolunteerRegistered($deliveryid, $userid);
  }

  public function isDeliveryFull(Request $request)
  {
  	$data = $request->input("pass");
    $dataArray = json_decode($data, true);

    $deliveryid = $dataArray["deliveryid"];

    return Database::isDeliveryFull($deliveryid);
  }

  public function isTrailerNeeded(Request $request)
  {
    $data = $request->input("pass");
    $dataArray = json_decode($data, true);

    $deliveryid = $dataArray["deliveryid"];

    return Database::isTrailerNeeded($deliveryid);
  }

  public function isPasswordValid(Request $request)
  {
    $data = $request->input("pass");
    $dataArray = json_decode($data, true);

    $password = $dataArray["password"];

    return Hash::check($password, Auth::user()->password);
  }
}