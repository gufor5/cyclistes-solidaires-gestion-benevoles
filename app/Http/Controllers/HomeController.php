<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Database;
use App\User;
use App\Validator;
use App\Enums\DeliveryFormParametersEnum;
use App\Objects\Delivery;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function home()
    {
        return view('home');
    }
}
