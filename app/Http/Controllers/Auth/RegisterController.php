<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Database;
use App\Enums\VolunteerAvailabilityEnum;
use App\Enums\VolunteerCapacityEnum;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

use App\Mail\VolunteerRegisterConfirmation;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:Volunteers'],
            'password' => ['required', 'min:8', 'max:255'],
            'phonenumber' => ['required', 'string', 'max:255'],
            'postalcode' => ['required', 'string', 'min:6', 'max:6'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $capacities = array();
        foreach (VolunteerCapacityEnum::allValues() as $capacity) 
        {
            if (in_array('capacity' . str_replace(' ', '', $capacity), array_keys($data)))
            {
                array_push($capacities, $capacity);
            }
        }

        $availabilities = array();
        foreach (VolunteerAvailabilityEnum::allValues() as $availability) 
        {
            if (in_array('availability' . str_replace(' ', '', $availability), array_keys($data)))
            {
                array_push($availabilities, $availability);
            }
        }

        $projects = array();
        foreach (Database::getEnabledProjects() as $project) 
        {
            if (in_array('project' . $project->getIdentifier(), array_keys($data)))
            {
                array_push($projects, $project->getIdentifier());
            }
        }

        $volunteer = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phonenumber' => $data['phonenumber'],
            'postalcode' => strtoupper($data['postalcode']),
            'capacity' => json_encode($capacities),
            'disponibilities' => json_encode($availabilities),
            'projects' => json_encode($projects),

        ]);

        Mail::to($volunteer->email)->send(new VolunteerRegisterConfirmation($volunteer));

        return $volunteer;
    }
}
