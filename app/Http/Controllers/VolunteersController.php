<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;

use App\Database;
use App\User;
use App\Validator;
use App\Enums\DeliveryFormParametersEnum;
use App\Enums\VolunteerFormParametersEnum;
use App\Enums\VolunteerAvailabilityEnum;
use App\Enums\VolunteerCapacityEnum;
use App\Objects\Delivery;
use App\Mail\VolunteerInformationUpdate;
use App\Mail\VolunteerDeleteConfirmation;

class VolunteersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function editVolunteer(Request $request)
    {
        return view('editVolunteer');
    }

    public function submitVolunteer(Request $request)
    {

        $capacities = array();
        foreach (VolunteerCapacityEnum::allValues() as $capacity) 
        {
            if ($request->has('capacity' . str_replace(' ', '', $capacity)))
            {
                array_push($capacities, $capacity);
            }
        }

        $availabilities = array();
        foreach (VolunteerAvailabilityEnum::allValues() as $availability) 
        {
            if ($request->has('availabilities' . str_replace(' ', '', $availability)))
            {
                array_push($availabilities, $availability);
            }
        }

        $projects = array();
        foreach (Database::getEnabledProjects() as $project) 
        {
            if ($request->has('projects' . str_replace(' ', '', $project->getIdentifier())))
            {
                array_push($projects, $project);
            }
        }

        if ($request->input(VolunteerFormParametersEnum::PASSWORD) != null)
        {
            $password = Hash::make($request->input(VolunteerFormParametersEnum::PASSWORD));
        }
        else
        {
            $password = Auth::user()->password;
        }

        $volunteer = new User();
        $volunteer->id = $request->input(VolunteerFormParametersEnum::ID);
        $volunteer->name = $request->input(VolunteerFormParametersEnum::NAME);
        $volunteer->email = $request->input(VolunteerFormParametersEnum::EMAIL);
        $volunteer->password = $password;
        $volunteer->phonenumber = $request->input(VolunteerFormParametersEnum::PHONENUMBER);
        $volunteer->postalcode = strtoupper($request->input(VolunteerFormParametersEnum::POSTALCODE));
        $volunteer->capacity = $capacities;
        $volunteer->disponibilities = $availabilities;
        $volunteer->projects = $projects;

        Database::updateVolunteer($volunteer);

        Mail::to($volunteer->email)->send(new VolunteerInformationUpdate($volunteer));

        return redirect(route('editVolunteer'));
    }

    public function validateVolunteer(Request $request)
    {
        $pass = $request->input("pass");
        $passArray = json_decode($pass, true);

        $validator = new Validator();
        $volunteer = new User();
        $volunteer->id = $passArray[VolunteerFormParametersEnum::ID];
        $volunteer->name = $passArray[VolunteerFormParametersEnum::NAME];
        $volunteer->email = $passArray[VolunteerFormParametersEnum::EMAIL];
        $volunteer->password = $passArray[VolunteerFormParametersEnum::PASSWORD];
        $volunteer->phonenumber = $passArray[VolunteerFormParametersEnum::PHONENUMBER];
        $volunteer->postalcode = $passArray[VolunteerFormParametersEnum::POSTALCODE];

        return json_encode($validator->validateVolunteer($volunteer));
    }

    public function deleteVolunteer(Request $request)
    {
        $user = User::find(Auth::user()->id);

        Auth::logout();

        foreach (Database::getDeliveriesAll() as $delivery) 
        {
            foreach ($delivery->getRegisteredVolunteers() as $volunteer) 
            {
                if ($user->id == $volunteer->id)
                {
                    Database::removeVolunteerToDelivery($delivery->getId(), $user->id);
                }
            }
        }

        Mail::to($user->email)->send(new VolunteerDeleteConfirmation($user));
        
        $user->delete();

        return redirect(route('home'));
    }
}
