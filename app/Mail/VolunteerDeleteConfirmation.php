<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VolunteerDeleteConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    protected $volunteer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($volunteer)
    {
        $this->volunteer = $volunteer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.volunteerDeleteConfirmation')
                    ->with([
                        'volunteer' => $this->volunteer
                    ]);
    }
}
