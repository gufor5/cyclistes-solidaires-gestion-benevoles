<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CoordonatorRemoveConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    protected $project;
    protected $volunteer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($project, $volunteer)
    {
        $this->project = $project;
        $this->volunteer = $volunteer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.coordonatorRemoveFromProject')
                    ->with([
                        'project' => $this->project,
                        'volunteer' => $this->volunteer
                    ]);
    }
}
