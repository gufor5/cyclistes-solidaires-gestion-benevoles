<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VolunteerRemoveFromDelivery extends Mailable
{
    use Queueable, SerializesModels;

    protected $delivery;
    protected $volunteer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($delivery, $volunteer)
    {
        $this->delivery = $delivery;
        $this->volunteer = $volunteer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.volunteerRemoveFromDelivery')
                    ->with([
                        'delivery' => $this->delivery,
                        'volunteer' => $this->volunteer
                    ]);
    }
}
