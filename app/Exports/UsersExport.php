<?php

namespace App\Exports;

use App\User;
use App\Database;
use App\Enums\VolunteerAvailabilityEnum;
use App\Enums\VolunteerCapacityEnum;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;

class UsersExport extends DefaultValueBinder implements FromArray, WithCustomStartCell, WithCustomValueBinder, ShouldAutoSize
{
    public function array(): array
    {
        $headers = array("NOM", "EMAIL");
        foreach (VolunteerAvailabilityEnum::allValues() as $availability) { array_push($headers, strtoupper($availability));}
        foreach (VolunteerCapacityEnum::allValues() as $capacity) { array_push($headers, strtoupper($capacity));}
        foreach (Database::getVisibleOnlyProjects() as $project) { array_push($headers, strtoupper($project->getName()));}

		$rows = array($headers);
		foreach (User::all() as $user) 
        {
            $line = array($user->name, $user->email);
            
            foreach (VolunteerAvailabilityEnum::allValues() as $availability) {
                array_push($line, in_array($availability, json_decode($user->disponibilities)));
            }

            foreach (VolunteerCapacityEnum::allValues() as $capacity) {
                array_push($line, in_array($capacity, json_decode($user->capacity)));
            }

            foreach (Database::getVisibleOnlyProjects() as $project) {
                array_push($line, in_array($project->getIdentifier(), json_decode($user->projects)));
            }

    		array_push($rows, $line);
		}

        return $rows;
    }

    public function startCell(): string
    {
        return 'B2';
    }

    public function bindValue(Cell $cell, $value)
    {
        $cell->setValueExplicit($value, DataType::TYPE_STRING);

        return true;
    }
}