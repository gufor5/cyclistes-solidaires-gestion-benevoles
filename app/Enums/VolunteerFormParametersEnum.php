<?php

  namespace App\Enums;

  class VolunteerFormParametersEnum
 	{
    private $value;

    const ID = "id";
    const NAME = "name";
    const EMAIL = "email";
    const PASSWORD = "password";
    const POSTALCODE = "postalcode";
    const PHONENUMBER = "phonenumber";
    const CAPACITY = "capacity";
    const AVAILABILITIES = "availabilities";
    const PROJECTS = "projects";

    public function __construct($value) {
      $this->value = $value;
    }

    public function getValue() {
      return $this->value;
    }
  }
