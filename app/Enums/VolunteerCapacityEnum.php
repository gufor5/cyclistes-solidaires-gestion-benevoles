<?php

  namespace App\Enums;

  class VolunteerCapacityEnum 
 	{
    private $value;

    const NONE = "Vélo uniquement";
    const BACKPACK = "Sac à dos";
    const CRATE = "Caisse de lait";
    const TOURING = "Sacoches de cyclotourisme";
    const TRAILER = "Remorque à vélo";

    public function __construct($value) 
    {
      $this->value = $value;
    }

    public static function allValues() 
    {
      return array(self::NONE, self::BACKPACK, self::CRATE, self::TOURING, self::TRAILER);
    }

    public function getValue() 
    {
      return $this->value;
    }

    public static function fromValue($value) 
    {
      foreach(self::allValues() as $capacity) 
      {
        if($capacity == $value) 
        {
          return new VolunteerCapacityEnum($value);
        }
      }
    }
  }
