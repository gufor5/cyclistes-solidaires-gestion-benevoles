<?php

  namespace App\Enums;

  class DeliveryFormParametersEnum 
 	{
    private $value;

    const ID = "id";
    const NAME = "name";
    const OWNER = "owner";
    const WHERE = "where";
    const WHEN = "when";
    const OBJECTIVE = "objective";
    const ESTIMATEDTIME = "time";
    const EQUIPMENTS = "equipements";
    const VOLUNTEERS_REGISTERED = "volunteers_registered";
    const VOLUNTEERS_IDEAL = "volunteers_ideal";
    const PROJECT = "projectidentifier";
    const COMMENT = "comment";
    const CONTACT_DELIVERY = "contact_delivery";

    public function __construct($value) {
      $this->value = $value;
    }

    public function getValue() {
      return $this->value;
    }
  }
