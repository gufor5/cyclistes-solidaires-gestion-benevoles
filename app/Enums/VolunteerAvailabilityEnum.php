<?php

  namespace App\Enums;

  class VolunteerAvailabilityEnum 
 	{
    private $value;

    const WEEKDAYS_AM = "Sur semaines en AM";
    const WEEKDAYS_PM = "Sur semaines en PM";
    const WEEKDAYS_EV = "Sur semaines en soirée";

    const WEEKENDS_AM = "Sur fin de semaines en AM";
    const WEEKENDS_PM = "Sur fin de semaines en PM";
    const WEEKENDS_EV = "Sur fin de semaines en soirée";

    public function __construct($value) {
      $this->value = $value;
    }

    public static function allValues() {
      return array(self::WEEKDAYS_AM, self::WEEKDAYS_PM, self::WEEKDAYS_EV, self::WEEKENDS_AM, self::WEEKENDS_PM, self::WEEKENDS_EV);
    }

    public static function getWeekendValues() {
      return array(self::WEEKENDS_AM, self::WEEKENDS_PM, self::WEEKENDS_EV); 
    }

    public static function getWeekdaysValues() {
      return array(self::WEEKDAYS_AM, self::WEEKDAYS_PM, self::WEEKDAYS_EV); 
    }

    public function getValue() {
      return $this->value;
    }
  }
