<?php

  namespace App\Enums;

  class DeliveryEquipmentsEnum 
 	{
    private $value;

    const NONE = "Aucun matériel requis, votre vélo sera suffisant!";
    const BACKPACK = "Un sac à dos est suffisant.";
    const PANNIERS = "Des sacoches à vélo sont requises.";
    const TRAILER = "Une remorque à vélo est requise.";

    public function __construct($value) {
      $this->value = $value;
    }

    public static function allValues() {
      return array(self::NONE, self::BACKPACK, self::PANNIERS, self::TRAILER);
    }

    public function getValue() {
      return $this->value;
    }
  }
