<?php

  namespace App\Enums;

  class ProjectStatusEnum 
 	{
    private $value;

    const ENABLED = "Enabled";
    const HIDDEN = "Hidden";
    const DISABLED = "Disabled";

    public function __construct($value) {
      $this->value = $value;
    }

    public static function allValues() {
      return array(self::ENABLED, self::DISABLED);
    }

    public function getValue() {
      return $this->value;
    }
  }
