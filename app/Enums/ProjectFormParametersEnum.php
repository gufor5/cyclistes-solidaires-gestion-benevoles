<?php

  namespace App\Enums;

  class ProjectFormParametersEnum
 	{
    private $value;

    const IDENTIFIER = "identifier";
    const NAME = "name";
    const COORDONATORS = "coordonators";

    public function __construct($value) {
      $this->value = $value;
    }

    public function getValue() {
      return $this->value;
    }
  }
