<?php 

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Objects\Project;
use App\Objects\Delivery;
use App\Enums\ProjectStatusEnum;
use App\Enums\DeliveryEquipmentsEnum;

class Database 
{
  const DB_PROJECT_TABLE = "Projects";
  const DB_PROJECT_NAME = "name";
  const DB_PROJECT_IDENTIFIER = "identifier";
  const DB_PROJECT_STATUS = "status";
  const DB_PROJECT_URL = "urlext";
  const DB_PROJECT_COORDINATORS = "coordinators";
  const DB_PROJECT_COORDINATORS_JSONKEY = "volunteers_id";

	const DB_DELIVERY_TABLE = "Deliveries";
	const DB_DELIVERY_ID = "id";
	const DB_DELIVERY_NAME = "name";
  const DB_DELIVERY_OWNER = "owner";
	const DB_DELIVERY_FROMWHERE = "fromwhere";
	const DB_DELIVERY_FROMWHEN = "fromwhen";
	const DB_DELIVERY_OBJECTIVE = "objective";
  const DB_DELIVERY_TIME = "estimatedtime";
	const DB_DELIVERY_EQUIPMENTS = "equipments";
	const DB_DELIVERY_VOLUNTEERS_REGISTERED = "volunteers_registered";
  const DB_DELIVERY_VOLUNTEERS_REGISTERED_JSONKEY = "volunteers_id";
	const DB_DELIVERY_VOLUNTEERS_IDEAL = "volunteers_ideal";
  const DB_DELIVERY_PROJECT = "projectidentifier";
	const DB_DELIVERY_COMMENT = "comment";
	const DB_DELIVERY_CONTACT_DELIVERY = "contact_delivery";

  const DB_VOLUNTEER_TABLE = "Volunteers";
  const DB_VOLUNTEER_ID = "id";
  const DB_VOLUNTEER_NAME = "name";
  const DB_VOLUNTEER_EMAIL = "email";
  const DB_VOLUNTEER_PHONE = "phonenumber";
  const DB_VOLUNTEER_POSTAL = "postalcode";
  const DB_VOLUNTEER_PASSWORD = "password";
  const DB_VOLUNTEER_CAPACITIES = "capacity";
  const DB_VOLUNTEER_DISPONIBILITIES = "disponibilities";
  const DB_VOLUNTEER_PROJECTS = "projects";

	public function __construct() 
  {
    
  }

  // VOLUNTEER RELATED METHODS

  public static function getVolunteer($email)
  {
    return User::where(self::DB_VOLUNTEER_EMAIL, $email)->first();
  }

  public static function updateVolunteer($volunteer)
  {
    $identifiers = array();
    foreach ($volunteer->projects as $project) {
      array_push($identifiers, $project->getIdentifier());
    }

    DB::table(self::DB_VOLUNTEER_TABLE)
      ->where(self::DB_VOLUNTEER_ID, $volunteer->id)
      ->update([
        self::DB_VOLUNTEER_NAME => $volunteer->name,
        self::DB_VOLUNTEER_EMAIL => $volunteer->email,
        self::DB_VOLUNTEER_PASSWORD => $volunteer->password,
        self::DB_VOLUNTEER_PHONE => $volunteer->phonenumber,
        self::DB_VOLUNTEER_POSTAL => $volunteer->postalcode,
        self::DB_VOLUNTEER_CAPACITIES => json_encode($volunteer->capacity),
        self::DB_VOLUNTEER_DISPONIBILITIES => json_encode($volunteer->disponibilities),
        self::DB_VOLUNTEER_PROJECTS => json_encode($identifiers)

    ]);
  }

  // PROJECT RELATED METHODS

  public static function getProject($projectidentifier)
  {
    $dbRow = DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_IDENTIFIER, $projectidentifier)
      ->first();

      return self::createProject($dbRow);
  }

  public static function getProjects()
  {
    $dbRows = DB::table(self::DB_PROJECT_TABLE)
      ->get();

    return self::createProjects($dbRows);
  }

  public static function getEnabledProjects()
  {
    $dbRows = DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_STATUS, ProjectStatusEnum::ENABLED)
      ->orWhere(self::DB_PROJECT_STATUS, ProjectStatusEnum::HIDDEN)
      ->orderBy(self::DB_PROJECT_NAME, 'asc')
      ->get();

    return self::createProjects($dbRows);
  }

  public static function getVisibleOnlyProjects()
  {
    $dbRows = DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_STATUS, ProjectStatusEnum::ENABLED)
      ->orderBy(self::DB_PROJECT_NAME, 'asc')
      ->get();

    return self::createProjects($dbRows);
  }

  public static function isProjectCoordinator($projectidentifier, $volunteerid)
  {
    $json = DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_IDENTIFIER, $projectidentifier)
      ->value(self::DB_PROJECT_COORDINATORS);

    $coordinatorids = self::decodeProjectCoordinatorIds($json);

    return in_array($volunteerid, $coordinatorids);
  }

  public static function isCoordinator($volunteerid)
  {
    foreach (self::getProjects() as $project) 
    {
      if (self::isProjectCoordinator($project->getIdentifier(), $volunteerid))
      {
        return true;
      }
    }

    return false;
  }

  public static function getProjectCoordos($projectidentifier)
  {
    $dbRow = DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_IDENTIFIER, $projectidentifier)
      ->value(self::DB_PROJECT_COORDINATORS);

    $coordos = self::decodeProjectCoordonators($dbRow, $projectidentifier);

    return $coordos;
  }

  public static function addCoordonatorToProject($projectidentifier, $volunteerid)
  {
    $oldjson = DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_IDENTIFIER, $projectidentifier)
      ->value(self::DB_PROJECT_COORDINATORS);

    $volunteerids = self::decodeProjectCoordinatorIds($oldjson);

    array_push($volunteerids, $volunteerid);

    $newjson = self::encodeProjectCoordinatorIds($volunteerids);

    DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_IDENTIFIER, $projectidentifier)
      ->update([self::DB_PROJECT_COORDINATORS => $newjson]);
  }

  public static function removeCoordonatorFromProject($projectidentifier, $volunteerid)
  {
    $oldjson = DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_IDENTIFIER, $projectidentifier)
      ->value(self::DB_PROJECT_COORDINATORS);

    $volunteerids = self::decodeProjectCoordinatorIds($oldjson);

    unset($volunteerids[array_search($volunteerid, $volunteerids)]);

    $newjson = self::encodeProjectCoordinatorIds($volunteerids);

    DB::table(self::DB_PROJECT_TABLE)
      ->where(self::DB_PROJECT_IDENTIFIER, $projectidentifier)
      ->update([self::DB_PROJECT_COORDINATORS => $newjson]);
  }

  // DELIVERY RELATED METHODS

  public static function getDelivery($deliveryid)
  {
    $dbRow = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->first();

      return self::createDelivery($dbRow);
  }

  public static function getDeliveriesAll()
  {
    $dbRows = DB::table(self::DB_DELIVERY_TABLE)
      ->get();

    return self::createDeliveries($dbRows);
  }

  public static function getDeliveries($projectidentifier) 
  {
  	$dbRows = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_PROJECT, $projectidentifier)
      ->orderBy(self::DB_DELIVERY_FROMWHEN, 'asc')
      ->get();

    return self::createDeliveries($dbRows);
  }

  public static function getDeliveriesAfterYesterday($projectidentifier)
  {
    $yesterday = date("Y-m-d", strtotime("yesterday"));

    $dbRows = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_FROMWHEN, '>=', $yesterday)
      ->where(self::DB_DELIVERY_PROJECT, $projectidentifier)
      ->orderBy(self::DB_DELIVERY_FROMWHEN, 'asc')
      ->get();

    return self::createDeliveries($dbRows);
  }

  public static function insertDelivery($delivery)
  {
    DB::table(self::DB_DELIVERY_TABLE)->insert([
        self::DB_DELIVERY_NAME => $delivery->getName(),
        self::DB_DELIVERY_OWNER => $delivery->getOwner()->id,
        self::DB_DELIVERY_FROMWHERE => $delivery->getFromWhere(),
        self::DB_DELIVERY_FROMWHEN => $delivery->getFromWhen(),
        self::DB_DELIVERY_OBJECTIVE => $delivery->getObjective(),
        self::DB_DELIVERY_TIME => $delivery->getEstimatedTime(),
        self::DB_DELIVERY_EQUIPMENTS => $delivery->getRequiredEquipments(),
        self::DB_DELIVERY_VOLUNTEERS_REGISTERED => self::encodeRegisteredVolunteersIds($delivery->getRegisteredVolunteers()),
        self::DB_DELIVERY_VOLUNTEERS_IDEAL => $delivery->getIdealVolunteerNumber(),
        self::DB_DELIVERY_PROJECT => $delivery->getProjectIdentifier(),
        self::DB_DELIVERY_COMMENT => $delivery->getComment(),
        self::DB_DELIVERY_CONTACT_DELIVERY => $delivery->getContactDelivery()
    ]);
  }

  public static function updateDelivery($delivery)
  {
    DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $delivery->getId())
      ->update([
        self::DB_DELIVERY_NAME => $delivery->getName(),
        self::DB_DELIVERY_FROMWHERE => $delivery->getFromWhere(),
        self::DB_DELIVERY_FROMWHEN => $delivery->getFromWhen(),
        self::DB_DELIVERY_OBJECTIVE => $delivery->getObjective(),
        self::DB_DELIVERY_TIME => $delivery->getEstimatedTime(),
        self::DB_DELIVERY_EQUIPMENTS => $delivery->getRequiredEquipments(),
        self::DB_DELIVERY_VOLUNTEERS_IDEAL => $delivery->getIdealVolunteerNumber(),
        self::DB_DELIVERY_PROJECT => $delivery->getProjectIdentifier(),
        self::DB_DELIVERY_COMMENT => $delivery->getComment(),
        self::DB_DELIVERY_CONTACT_DELIVERY => $delivery->getContactDelivery()

    ]);
  }

  public static function removeDelivery($deliveryid)
  {
    DB::table(self::DB_DELIVERY_TABLE)->where(self::DB_DELIVERY_ID, $deliveryid)->delete();
  }

  public static function isDeliveryExist($deliveryid)
  {
    $deliveryids = DB::table(self::DB_DELIVERY_TABLE)->pluck(self::DB_DELIVERY_ID)->toArray();

    return in_array($deliveryid, $deliveryids);
  }

  public static function addVolunteerToDelivery($deliveryid, $volunteerid)
  {
    $oldjson = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->value(self::DB_DELIVERY_VOLUNTEERS_REGISTERED);

    $volunteerids = self::decodeRegisteredVolunteersIds($oldjson);

    array_push($volunteerids, $volunteerid);

    $newjson = self::encodeRegisteredVolunteersIds($volunteerids);

    DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->update([self::DB_DELIVERY_VOLUNTEERS_REGISTERED => $newjson]);
  }

  public static function removeVolunteerToDelivery($deliveryid, $volunteerid)
  {
    $oldjson = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->value(self::DB_DELIVERY_VOLUNTEERS_REGISTERED);

    $volunteerids = self::decodeRegisteredVolunteersIds($oldjson);

    unset($volunteerids[array_search($volunteerid, $volunteerids)]);

    $newjson = self::encodeRegisteredVolunteersIds($volunteerids);

    DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->update([self::DB_DELIVERY_VOLUNTEERS_REGISTERED => $newjson]);
  }

  public static function isVolunteerRegistered($deliveryid, $volunteerid)
  {
    $json = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->value(self::DB_DELIVERY_VOLUNTEERS_REGISTERED);

    $volunteerids = self::decodeRegisteredVolunteersIds($json);

    return in_array($volunteerid, $volunteerids);
  }

  public static function isDeliveryFull($deliveryid)
  {
    $json = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->value(self::DB_DELIVERY_VOLUNTEERS_REGISTERED);

    $registeredids = self::decodeRegisteredVolunteersIds($json);

    $ideal = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->value(self::DB_DELIVERY_VOLUNTEERS_IDEAL);

    if (sizeof($registeredids) >= $ideal) { return true; }
    else                                  { return false; }
  }

  public static function isTrailerNeeded($deliveryid)
  { 
    $equipment = DB::table(self::DB_DELIVERY_TABLE)
      ->where(self::DB_DELIVERY_ID, $deliveryid)
      ->value(self::DB_DELIVERY_EQUIPMENTS);

    if ($equipment == DeliveryEquipmentsEnum::TRAILER) { return true;  }
    else                                               { return false; }
  }

  // PROJECTS PRIVATE UTILITY METHODS

  private static function createProjects($dbRows)
  {
    $projects = array();

    foreach ($dbRows as $dbRow) 
    {
      array_push($projects, self::createProject($dbRow));
    }

    return $projects;
  }

  private static function createProject($dbRow)
  {
    $project = new Project(
        $dbRow->{self::DB_PROJECT_IDENTIFIER},
        $dbRow->{self::DB_PROJECT_NAME},
        $dbRow->{self::DB_PROJECT_STATUS},
        $dbRow->{self::DB_PROJECT_URL}
      );

    return $project;
  }

  private static function decodeProjectCoordonators($json, $projectidentifier)
  {
    $coordos = array();
    $ids = self::decodeProjectCoordinatorIds($json);

    foreach ($ids as $id) 
    {
      if (User::find($id) != null)
      {
        array_push($coordos, User::find($id));
      }
      else
      {
        self::removeCoordonatorFromProject($projectidentifier, $id);
      }
    }

    return $coordos;
  }

  private static function decodeProjectCoordinatorIds($json)
  {
    $ids = array();
    $array = json_decode($json);

    foreach ($array->{self::DB_PROJECT_COORDINATORS_JSONKEY} as $id) 
    {
      array_push($ids, $id);  
    }

    return $ids;
  }

  private static function encodeProjectCoordinatorIds($ids)
  {
    return json_encode(array(self::DB_PROJECT_COORDINATORS_JSONKEY => $ids));
  }

  // DELIVRIES PRIVATE UTILITY METHODS

  private static function createDeliveries($dbRows)
  {
    $deliveries = array();

    foreach ($dbRows as $dbRow) 
    {
      if (User::where('id', $dbRow->{self::DB_DELIVERY_OWNER})->first() == null)
      {
        self::removeDelivery($dbRow->{self::DB_DELIVERY_ID});
      }
      else
      {
        array_push($deliveries, self::createDelivery($dbRow));
      }
      
    }

    return $deliveries;
  }

  private static function createDelivery($dbRow)
  {
    return new Delivery(
        $dbRow->{self::DB_DELIVERY_ID},
        $dbRow->{self::DB_DELIVERY_NAME},
        User::where('id', $dbRow->{self::DB_DELIVERY_OWNER})->first(),
        $dbRow->{self::DB_DELIVERY_FROMWHERE},
        $dbRow->{self::DB_DELIVERY_FROMWHEN},
        $dbRow->{self::DB_DELIVERY_OBJECTIVE},
        $dbRow->{self::DB_DELIVERY_TIME},
        $dbRow->{self::DB_DELIVERY_EQUIPMENTS},
        self::decodeRegisteredVolunteers($dbRow->{self::DB_DELIVERY_VOLUNTEERS_REGISTERED}, $dbRow->{self::DB_DELIVERY_ID}),
        $dbRow->{self::DB_DELIVERY_VOLUNTEERS_IDEAL},
        $dbRow->{self::DB_DELIVERY_PROJECT},
        $dbRow->{self::DB_DELIVERY_COMMENT},
        $dbRow->{self::DB_DELIVERY_CONTACT_DELIVERY},
      );
  }

  private static function decodeRegisteredVolunteers($json, $deliveryid)
  {
    $volunteers = array();
    $ids = self::decodeRegisteredVolunteersIds($json);

    foreach ($ids as $id) 
    {
      if (User::find($id) != null)
      {
        array_push($volunteers, User::find($id));
      }
      else
      {
        self::removeVolunteerToDelivery($deliveryid, $id);
      }
    }

    return $volunteers;
  }

  private static function decodeRegisteredVolunteersIds($json)
  {
    $ids = array();
    $array = json_decode($json);

    foreach ($array->{self::DB_DELIVERY_VOLUNTEERS_REGISTERED_JSONKEY} as $id) 
    {
      array_push($ids, $id);  
    }

    return $ids;
  }

  private static function encodeRegisteredVolunteersIds($ids)
  {
    return json_encode(array(self::DB_DELIVERY_VOLUNTEERS_REGISTERED_JSONKEY => $ids));
  }
}