<?php

namespace App\Objects;

class Project {

	private $identifier;
  private $name;
  private $status;
  private $url;

  public function __construct($identifier, $name, $status, $url) 
  {
  	$this->identifier = $identifier;
    $this->name = $name;
    $this->status = $status;
    $this->url = $url;
  }

  public function getIdentifier() { return $this->identifier; }

  public function getName() { return $this->name; }

  public function getStatus() { return $this->status; }

  public function getUrl() { return $this->url; }
}
