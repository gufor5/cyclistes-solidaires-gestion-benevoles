<?php

namespace App\Objects;

class Delivery {

	private $id;
	private $name;
  private $owner;
  private $fromWhere;
  private $fromWhen;
  private $objective;
  private $estimatedTime;
  private $requiredEquipments;
  private $registeredVolunteers;
  private $idealVolunteerNumber;
  private $projectidentifier;
  private $comment;
  private $contactdelivery;

  public function __construct($id, $name, $owner, $fromWhere, $fromWhen, $objective, $estimatedTime, $requiredEquipments, $registeredVolunteers, $idealVolunteerNumber, $projectidentifier, $comment, $contactdelivery) 
  {
  	$this->id = $id;
    $this->name = $name;
    $this->owner = $owner;
    $this->fromWhere = $fromWhere;
    $this->fromWhen = $fromWhen;
    $this->objective = $objective;
    $this->estimatedTime = $estimatedTime;
    $this->requiredEquipments = $requiredEquipments;
    $this->registeredVolunteers = $registeredVolunteers;
    $this->idealVolunteerNumber = $idealVolunteerNumber;
    $this->projectidentifier = $projectidentifier;
    $this->comment = $comment;
    $this->contactdelivery = $contactdelivery;
  }

  public function getId() { return $this->id; }

  public function getName() { return $this->name; }

  public function getOwner() { return $this->owner; }

  public function getFromWhere() { return $this->fromWhere; }

  public function getFromWhen() { return $this->fromWhen; }

  public function getObjective() { return $this->objective; }

  public function getEstimatedTime() { return $this->estimatedTime; }

  public function getRequiredEquipments() { return $this->requiredEquipments; }

  public function getRegisteredVolunteers() { return $this->registeredVolunteers; }

  public function getIdealVolunteerNumber() { return $this->idealVolunteerNumber; }

  public function getProjectIdentifier() { return $this->projectidentifier; }

  public function getComment() { return $this->comment; }

  public function getContactDelivery() { return $this->contactdelivery; }
}
