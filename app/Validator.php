<?php

  namespace App;

  use Illuminate\Support\Facades\Auth;

  use App\Enums\DeliveryFormParametersEnum;
  use App\Enums\VolunteerFormParametersEnum;

  class Validator 
  {
    const STATUS = "status";
    const FEEDBACK = "feedback";
    const EMPTY = "";

    const MESSAGE_COORDONATOR_EMAIL_DUPLICATA = "Déjà un coordonateur!";
    const MESSAGE_COORDONATOR_EMAIL_MISSING = "Adresse email inexistante!";
    const MESSAGE_VOLUNTEER_NAME_DUPLICATA = "Ce nom est déjà pris!";
    const MESSAGE_VOLUNTEER_EMAIL_DUPLICATA = "Ce email est déjà pris!";
    const MESSAGE_VOLUNTEER_PASSWORD_SHORT = "Le mot de passe doit comprendre au moins 8 charactères";
    const MESSAGE_NMB_NEGATIVE = "Doit être plus grand que zéro!";
    const MESSAGE_NMB_WRONGTYPE = "Doit être un nombre!";
    const MESSAGE_STR_TOOLONG = "Trop long!";
    const MESSAGE_STR_TOOSHORT = "Trop court!";
    const MESSAGE_STR_WRONGTYPE = "Doit être une phrase!";
    const MESSAGE_DATE_BADFORMAT = "Mauvais format!";
    const MESSAGE_DATE_PAST = "Date future requise!";
    const MESSAGE_REQUIRED = "Requis!";
    const MESSAGE_NONE = "";

    const PASSWORD_MINIMUM_LENGTH = 8;

    private $pass;

    public function __construct() {
      $this->pass = [];
    }

    public function validateDelivery($delivery)
    {
      $this->pass[DeliveryFormParametersEnum::NAME] = $this->validateString($delivery->getName());
      $this->pass[DeliveryFormParametersEnum::WHERE] = $this->validateString($delivery->getFromWhere());
      $this->pass[DeliveryFormParametersEnum::WHEN] = $this->validateDate($delivery->getFromWhen());
      $this->pass[DeliveryFormParametersEnum::OBJECTIVE] = $this->validateString($delivery->getObjective());
      $this->pass[DeliveryFormParametersEnum::ESTIMATEDTIME] = $this->validateNumGreaterThanZero($delivery->getEstimatedTime());
      $this->pass[DeliveryFormParametersEnum::EQUIPMENTS] = $this->validateString($delivery->getRequiredEquipments());
      $this->pass[DeliveryFormParametersEnum::VOLUNTEERS_IDEAL] = $this->validateNumGreaterThanZero($delivery->getIdealVolunteerNumber());

      return $this->pass;
    }

    public function validateVolunteer($volunteer)
    {
      $this->pass[VolunteerFormParametersEnum::NAME] = $this->validateString($volunteer->name);
      $this->pass[VolunteerFormParametersEnum::EMAIL] = $this->validateVolunteerEmail($volunteer->email);
      $this->pass[VolunteerFormParametersEnum::PASSWORD] = $this->validateVolunteerPassword($volunteer->password);
      $this->pass[VolunteerFormParametersEnum::PHONENUMBER] = $this->validateNotEmpty($volunteer->phonenumber);
      $this->pass[VolunteerFormParametersEnum::POSTALCODE] = $this->validateStringLength($volunteer->postalcode, 6);

      return $this->pass;
    }

    public function validateCoordonatorAdd($coordos, $volunteer)
    {
      $this->pass[VolunteerFormParametersEnum::EMAIL] = $this->validateCoordonatorAddEmail($coordos, $volunteer);

      return $this->pass;
    }

    // VOLUNTEER TEST METHODS

    private function validateVolunteerEmail($value)
    {
      if(strlen($value) > 255) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_STR_TOOLONG);
      }
      elseif(trim($value) == self::EMPTY) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_REQUIRED);
      }
      else {

        foreach (User::all() as $user) 
        {
          if ($user->email == $value && Auth::user()->id != $user->id)
          {
            return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_VOLUNTEER_EMAIL_DUPLICATA);
          }
        }

        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
    }

    private function validateVolunteerPassword($value)
    {
      if(strlen($value) > 255) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_STR_TOOLONG);
      }
      elseif(trim($value) == self::EMPTY) {
        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
      elseif(strlen($value) < self::PASSWORD_MINIMUM_LENGTH)
      {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_VOLUNTEER_PASSWORD_SHORT);
      }
      else {
        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
    }

    private function validateCoordonatorAddEmail($coordos, $volunteer)
    {
      foreach ($coordos as $coordo) 
      {
        if ($coordo->email == $volunteer->email)
        {
          return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_COORDONATOR_EMAIL_DUPLICATA);
        }
      }

      foreach (User::all() as $user) 
      {
        if ($user->email == $volunteer->email)
        {
          return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
        }
      }

      return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_COORDONATOR_EMAIL_MISSING);
    }

    // COMMON TEST METHODS

    private function validateNotEmpty($value)
    {
      if(trim($value) == self::EMPTY) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_REQUIRED);
      }
      else {
        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
    }

    private function validateString($value) 
    {
      if(strlen($value) > 255) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_STR_TOOLONG);
      }
      elseif(is_numeric($value)) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_STR_WRONGTYPE);
      }
      elseif(trim($value) == self::EMPTY) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_REQUIRED);
      }
      else {
        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
    }

    private function validateStringLength($value, $lenth)
    {
      if(strlen($value) > $lenth)
      {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_STR_TOOLONG);
      }
      elseif(strlen($value) < $lenth)
      {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_STR_TOOSHORT);
      }
      else {
        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
    }

    private function validateNumGreaterThanZero($value) 
    {
      $numeric = is_numeric($value);
      $greaterThanZero = floatVal($value) > 0;

      if(trim($value) == self::EMPTY) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_REQUIRED);
      }
      else if(!$numeric) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_NMB_WRONGTYPE);
      }
      else if(!$greaterThanZero) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_NMB_NEGATIVE);
      }
      else {
        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
    }

    private function validateDate($value)
    {
      $regex = "/^[0-9]{4}+-[0-1]{1}+[0-9]{1}+-[0-3]{1}+[0-9]{1}+[ ]{1}+[0-2]{1}+[0-9]{1}+:[0-5]{1}+[0-9]{1}$/";

      if(!(preg_match($regex, $value))) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_DATE_BADFORMAT);
      }
      else if(strtotime($value) <= strtotime("yesterday")) {
        return array(self::STATUS => false, self::FEEDBACK=>self::MESSAGE_DATE_PAST);
      }
      else {
        return array(self::STATUS => true, self::FEEDBACK=>self::MESSAGE_NONE);
      }
    }
  }